@extends('layout.master')
@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
             <h3 class="card-title">Create New Profile</h3>
        </div>
        <form role="form" action="/profile" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="bio">Biodata</label>
                    <input type="text" class="form-control" id="biodata" name="biodata" placeholder="Enter Bio">
                    @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email"  placeholder="Enter Email">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                </div>
                <div class="form-group">
                        <label for="age">Age</label>
                        <input type="text" class="form-control" id="umur" name="umur"  placeholder="Enter Age">
                        @error('age')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                </div>
                <div class="form-group">
                        <label for="alamat">Address</label>
                        <input type="text" class="form-control"  name="alamat"  placeholder="Enter Profile">
                        @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                </div>
                </div>
                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
        </form>
    </div>`
</div>
@endsection
