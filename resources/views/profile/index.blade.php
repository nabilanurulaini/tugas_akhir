@extends('layout.master')
@section('title')
Show profile
@endsection
@section('content')
<a href="/profile/create" class="form-control btn btn-primary mb-2" >Add</a>
<table class="mt-2 table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Biodata</th>
        <th scope="col">Email</th>
        <th scope="col">Age</th>
        <th scope="col">Address</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($profile as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->biodata}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->alamat}}</td>
                <td>
                    <a href="/profile/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/profile/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/profile/{{$value->id}}" style="display: inline" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        {{-- if there's no data anymore then  --}}
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection
