@extends('layout.master')
@section('title')
    Question
@endsection
@section('content')
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Question Table</h3>
        </div>
              <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="/pertanyaan/create">Add Question</a>
            <div class="row">
                @forelse ($pertanyaan as $item)
                <div class="col-2 mt-2">
                    <div class="card" >
                        <img src="{{asset('images/'.$item->gambar)}}"  alt="{{$item->judul}}">
                        <div class="card-body">
                            <h5 class="card-title">{{$item->judul}}</h5>
                            <p class="card-text">{{$item->konten}}</p>


                        <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/pertanyaan/{{$item->id}}" style="display: inline" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                        <a href="/pertanyaan/{{$item->id}}" class="btn btn-success mt-2">Read More</a>
                        </div>
                    </div>
                </div>
                @empty
                    No Question

                @endforelse
            </div>
        </div>
    </div>
</div>


@endsection
