@extends('layout.master')
@section('title')
Edit pertanyaan {{$pertanyaan->id}}
@endsection
@section('content')
{{-- //enctype supaya file-file yg diupload bisa masul --}}
<form action="/pertanyaan/{{$pertanyaan->id}}" method="POST" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="form-group">
        <label >Title</label>
        <input type="text" class="form-control mb-2" name="judul" value="{{$pertanyaan->judul}}"  placeholder="Please insert title">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="body">Summary</label>
        <textarea class="form-control mb-2" name="konten" value="{{$pertanyaan->konten}}" placeholder="Please insert question" ></textarea>
        @error('konten')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="body">Year</label>
        <input type="file" class="form-control mb-2" name="gambar" value="{{$pertanyaan->gambar}}">
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">kategori</label>
        <select class="form-control mb-2" name="kategori_id">
            <option>--Choose kategori--</option>

            @foreach ($kategori as $item)
                @if ($item->id === $pertanyaan->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
