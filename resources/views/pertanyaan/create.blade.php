@extends('layout.master')
@section('title')
Add Question
@endsection
@section('content')
{{-- //enctype supaya file-file yg diupload bisa masul --}}
<form action="/pertanyaan" method="POST" enctype="multipart/form-data">
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create Question</h3>
            </div>
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <a class="btn btn-primary mb-2" href="/kategori/create">Create New Questionn</a>
                <div class="form-group">
                    <label >Title</label>
                    <input type="text" class="form-control mb-2" name="judul"  placeholder="Please insert title">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="konten">Content</label>
                    <textarea name="konten"  class="form-control mb-2" id="konten" placeholder="Please insert content"></textarea>
                    @error('konten')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="body">Picture</label>
                    <input type="file" class="form-control mb-2" name="gambar">
                    @error('gambar')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">category</label>
                    <select class="form-control mb-2" name="kategori_id">
                        <option>--Choose Category--</option>
                        @foreach ($kategori as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('kategori_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
    </div>
    @csrf

@endsection
