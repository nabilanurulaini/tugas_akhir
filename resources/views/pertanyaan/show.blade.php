@extends('layout.master')
@section('title')
{{$pertanyaan->judul}}
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card" >
            <img src="{{asset('images/'.$pertanyaan->gambar)}}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">{{$pertanyaan->judul}}</h5>
                <p class="card-text">{{Str::limit($pertanyaan->konten, 50)}}</p>

            <a href="/pertanyaan" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
    {{-- @if($item->id == $genre->genre_id)
        <option value="{{$item->id}}" selected>{{$item->nama}}</option>

    @else
        <option value="{{$item->id}}">{{$item->nama}}
    @endif --}}
</div>
@endsection
