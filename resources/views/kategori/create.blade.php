@extends('layout.master')
@section('content')
<h2>Add Category</h2>
<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Category</label>
        <input type="text" class="form-control mb-2" name="nama" id="nama" placeholder="Please insert category">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea name="deskripsi"  class="form-control mb-2" id="deskripsi" placeholder="Please insert description"></textarea>
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection
