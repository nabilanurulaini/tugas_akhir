@extends('layout.master')
@section('title')
Edit Category {{$kategori->nama}}
@endsection
@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label >Category</label>
        <input type="text" class="form-control" value= "{{$kategori->nama}}" name="nama"  placeholder="Please insert category name">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label >Description</label>
        <input type="text" class="form-control" value= "{{$kategori->deskripsi}}" name="deskripsi"  placeholder="Please insert description">
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
