@extends('layout.master')
@section('title')
{{$kategori->nama}} Details
@endsection
@section('content')
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
        <h2>{{$kategori->nama}} Category</h2>
        <p>{{$kategori->deskripsi}}</p>
        </div>
    </div>
</div>
@endsection
