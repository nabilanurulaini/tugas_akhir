@extends('layout.master')
@section('content')

<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Category Table</h3>
        </div>
              <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="/kategori/create">Add Category</a>
                <table class="mt-2 table">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Category</th>
                        <th scope="col">Description</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($kategori as $key=>$value)
                            <tr>
                                <td>{{$key + 1}}</th>
                                <td>{{$value->nama}}</td>
                                <td>{{$value->deskripsi}}</td>
                                <td>
                                    <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a>
                                    <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/kategori/{{$value->id}}" style="display: inline" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        {{-- if there's no data anymore then  --}}
                        @empty
                            <tr colspan="3">
                                <td>No data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
              </div>
            </div>
    </div>

    @endsection
