<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profile;
use Auth;

class profileController extends Controller
{
    //
    public function index()
    {

        $profile = profile::all();
        return view('profile.index', compact('profile'));
        // $profile = profile::where('user_id', Auth::id())->first();
        // return view('profile.index', compact('profile'));
    }
    public function create()
    {
        return view('profile.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'biodata' => 'required',
            'email' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
            'user_id' => 'required',

        ]);

        $profile = new profile;
        $profile->biodata = $request->biodata;
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->user_id = $request->user_id;
        $profile->save();

        return redirect('/profile');
    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            'biodata' => 'required',
            'email' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
            'user_id' => 'required',
        ]);
        $profile = profile::find($id);
        $profile->biodata = $request->biodata;
        $profile->email = $request->email;
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->user_id = $request->user_id;
        $profile->update();

        return redirect('/profile');
    }
     public function destroy($id)
    {
        //delete data with method delete() using method find($id) from database
         profile::destroy($id);
         return redirect('/profile')->with('success', 'Profil Telah Dihapus!');
    }
}

