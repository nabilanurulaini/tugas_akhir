<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;

class kategoriController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:255',
            'deskripsi' => 'required|max:255'
        ]);

       kategori::insert([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi']
        ]);

        return redirect('kategori');
    }
    public function index()
    {
        $kategori = kategori::all();
        return view('kategori.index', compact('kategori'));
    }
    public function show($id)
    {
        //ambil yg pertama, pakai array kalau tidak;get untuk banyak foreach atau nembak array
        $kategori = kategori::find($id);

        return view('kategori.show', compact('kategori'));
    }
    public function edit($id)
    {
        //ambil yg pertama, pakai array kalau tidak;get untuk banyak foreach atau nembak array
        $kategori = kategori::find($id);

        return view('kategori.edit', compact('kategori'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama' => 'required|max:255',
            'deskripsi' => 'required|max:255'
        ]);

        $kategori = kategori::find($id);
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->update();

        return redirect('kategori');
    }
    public function destroy($id)
    {
        $kategori = kategori::find($id);
        $kategori->delete();
        return redirect('/kategori');

    }
}
