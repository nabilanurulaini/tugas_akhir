<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;
use App\pertanyaan;

use File;
class pertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('auth')->except(['index', 'show']);

    }

    public function create()
    {
        $kategori = kategori::get();
        return view('pertanyaan.create',compact('kategori'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'judul' => 'required',
            'konten' => 'required',
            'gambar' => 'required|image|mimes:jpeg, jpg, gif, png, svg|max : 2048',
            'kategori_id' => 'required'
        ]);
        $gambarName = time().'.'.$request->gambar->extension();//rake the time from image when it's uploaded
        $request->gambar->move(public_path('images'),$gambarName);//export the file to public folder > images
        $pertanyaan = new pertanyaan;

        $pertanyaan->judul    =$request->judul;
        $pertanyaan->konten   =$request->konten;
        $pertanyaan->gambar   =$gambarName;
        $pertanyaan->kategori_id =$request->kategori_id;

        $pertanyaan->save();
        return redirect('/pertanyaan');
    }
    public function index()
    {

        $pertanyaan = pertanyaan::all();//get all data from database using eloquent method
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function show($id)
    {
        //ambil yg pertama, pakai array kalau tidak;get untuk banyak foreach atau nembak array
        $pertanyaan = pertanyaan::find($id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id)
    {
        //ambil yg pertama, pakai array kalau tidak;get untuk banyak foreach atau nembak array
        //$film = DB::table('film')->where('id', $id)->first();
        $kategori = kategori::get();
        $pertanyaan = pertanyaan::find($id);
        return view('pertanyaan.edit', compact('pertanyaan', 'kategori'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'judul' => 'required',
            'konten' => 'required',
            'gambar' => 'required|image|mimes:jpeg, jpg, gif, png, svg|max : 2048',
            'kategori_id' => 'required'
        ]);

        $gambarName = time().'.'.$request->gambar->extension();//rake the time from image when it's uploaded
        $request->gambar->move(public_path('images'),$gambarName);//export the file to public folder > images
        $pertanyaan = pertanyaan::findorfail($id);
        if ($request->has('gambar')) {
            $path = "/images";
            File::delete($path . $pertanyaan->gambar);
            $image = $request->gambar;
            $replacedImage = time() . ' - '. $image->getClientOriginalName();
            $image->move('images/',$replacedImage);
            $dataPertanyaan = [
                'judul' =>$request->judul,
                'konten' =>$request->konten,
                'gambar' =>$request->gambar,
                'kategori_id' =>$request->kategori_id
            ];
        } else {
            $dataPertanyaan = [
                'judul' =>$request->judul,
                'konten' =>$request->konten,
                'kategori_id' =>$request->kategori_id
            ];

        }

        $pertanyaan->update($dataPertanyaan);

        return redirect('/pertanyaan');
    }
    public function destroy($id)
    {
        //delete data with method delete() using method find($id) from database
        // $film = film::find($id);
        // $film->delete();
        // return redirect('/film');
        $pertanyaan = pertanyaan::findorfail($id);
        $pertanyaan->delete();
        $path = "/images";
        File::delete($path .$pertanyaan->gambar);
        return redirect('/pertanyaan');

    }
}
