<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class jawabanController extends Controller
{
    public function create(){
        $pertanyaan = Question::get();
        return view('jawaban.create',compact('pertanyaan'));
    }

    public function store(Request $request){

           $request->validate([
               'jawaban' => 'required',
               'pertanyaan_id' => 'required',
           ]);
           $jawaban= new jawaban;
           $jawaban->jawaban = $request->jawaban;
           $jawaban->pertanyaan_id = $request->pertanyaan_id;
           $jawaban->save();

           return redirect('/jawaban')->with('success', 'Sucessfull!');
        }

        public function index()
         {

            $jawaban = jawaban::all();
            return view('jawaban.index', compact('jawaban'));
        }

        public function show($id)
        {

            $jawaban = jawaban::find($id);
            return view('jawaban.show', compact('jawaban'));
        }

        public function edit($id)
        {

            $jawaban = pertanyaan::find($id);
            return view('jawaban.edit', compact('jawaban'));
        }

        public function update($id, Request $request)
        {
            $request->validate([
                'jawaban' => 'required',
                'pertanyaans_id' => 'required',

            ]);
            $update = jawaban::where('id', $id)->update([
                "jawaban" => $request["jawaban"],
                "pertanyaan_id" => $request["pertanyaan_id"],
            ]);
            return redirect('/jawaban')->with('success', 'Berhasil Update jawaban!');
        }

        public function destroy($id){

            jawaban::destroy($id);
            return redirect('/jawaban')->with('success', 'Answer deleted!');
        }
}

