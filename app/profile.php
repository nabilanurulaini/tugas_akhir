<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\profile as Authenticatable;
class profile extends Model
{
    protected $table = 'profile';

    protected $fillable = ['biodata','email','umur','alamat'];
}
