<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/home', function () {
    return view('layout.master');
});
Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::resource('/kategori', 'kategoriController');
    Route::resource('profile', 'profileController')->only([
        'index', 'update'
    ]);
});
Route::resource('/pertanyaan', 'pertanyaanController');
Route::resource('/jawaban', 'jawabanController');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::POST('/profile', 'profileController@store');
// Route::GET('/profile/create', 'profileController@create');
// Route::GET('/profile', 'profileController@index');
//  Route::group(['middleware' => ['auth']], function () {


//     Route::POST('/profile', 'profileController@store');
//     Route::GET('/profile/create', 'profileController@create');
//     Route::GET('/profile', 'profileController@index');
// //     // Route::get('/profile', 'profileController@index')->name('home');
// });
